/*
 *  AggregatePrimitive.cpp
 *
 */

#include "Scene.h"
#include "Shape.h"
#include "LocalGeo.h"
#include "AggregatePrimitive.h"
#include "Intersection.h"


#include "Ray.h"
#include "glm/glm.hpp"


AggregatePrimitive::AggregatePrimitive(Scene &scene_init){
    s = scene_init;
}

AggregatePrimitive::AggregatePrimitive(){}


bool AggregatePrimitive::intersect(Ray ray, float *thit, Intersection &intsec){
    
    float lowest_t = 10000000;
    LocalGeo loc_geo = LocalGeo();
    bool hit_shape = false;
    
    for (int i = 0; i < s.tri_v1.size(); i++) {
        
        Triangle triangle = Triangle(s.tri_v1[i],
                                     s.tri_v2[i],
                                     s.tri_v3[i],
                                     s.tri_inv[i],
                                     s.tri_emission[i],
                                     s.tri_ambient[i],
                                     s.tri_diffuse[i],
                                     s.tri_specular[i],
                                     s.tri_reflect[i]);

        if (triangle.intersect(ray, thit, loc_geo)) {
            
            float dist = pow(ray.pos[0] - loc_geo.pos[0], 2) + 
                        pow(ray.pos[1] - loc_geo.pos[1], 2) + 
                        pow(ray.pos[2] - loc_geo.pos[2], 2);
            
            if (dist < lowest_t) {
                intsec.local_geo = loc_geo;
                intsec.shape = triangle;
                
                lowest_t = dist;
                
            }
            hit_shape = true;
        } 
        
    }
    
    
    for (int i = 0; i < s.spheres.size(); i++) {
        
        //cout << "Sphere: " << i << endl;
        //TODO: move this to better part of the code, probably intersect code
        
        // make Sphere out of sphere vector coming from spheres
        Sphere sphere = Sphere(s.spheres[i], 
                               s.sphere_inv[i], 
                               s.sphere_emission[i],
                               s.sphere_ambient[i],
                               s.sphere_diffuse[i],
                               s.sphere_specular[i],
                               s.sphere_reflect[i]);
        
        if (sphere.intersect(ray, thit, loc_geo)) {
            
            float dist = pow(ray.pos[0] - loc_geo.pos[0], 2) + 
                         pow(ray.pos[1] - loc_geo.pos[1], 2) + 
                         pow(ray.pos[2] - loc_geo.pos[2], 2);
            
            if (dist < lowest_t) {
                intsec.local_geo = loc_geo;
                intsec.shape = sphere;
                
                lowest_t = dist;
                
            }
            hit_shape = true;
        } 
        
    }
    
    return hit_shape;

}
