/*
 *  AggregatePrimitive.h
 */

class Scene;
class Color;
class Ray;
class Intersection;
class AggregatePrimitive;

class AggregatePrimitive{
    private:
        Scene s;
    public:
        AggregatePrimitive(Scene &scene_init);
        AggregatePrimitive();
        ~AggregatePrimitive(){};
        bool intersect(Ray ray, float *thit, Intersection &intsec);
};
