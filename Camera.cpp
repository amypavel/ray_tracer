#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"
using namespace std;


#include "Scene.h"
#include "Ray.h" 
#include "Sample.h"

#include "Camera.h"

// main task is to create a ray starting from the 
// camera that passes through the corresponding
// pixel (sample.x, sample.y) on the image plane
Camera::Camera() {}

Camera::Camera(Scene& sc) {
    s = sc;
}

void Camera::generateRay(Sample &sample, Ray &ray){
    
    glm::vec3 w = glm::normalize(s.lookfrom - s.lookat);
    glm::vec3 u = glm::normalize(glm::cross(s.up, w));
    glm::vec3 v = glm::cross(w, u);
    
    float a = s.width/s.height;

    float hi = 2 * tan(s.fovv/2 * (M_PI/180));
    float wi = a * hi;
    
    float cu = (sample.x + 0.5f) * wi / s.width - (0.5f * wi);
    float cv = (sample.y + 0.5f) * hi / s.height - (0.5f * hi);
    
    // find the middle of the image plane
    float td = 1.0f/sqrt(pow(s.lookfrom.x - s.lookat.x,2) + 
                         pow(s.lookfrom.y - s.lookat.y,2) + 
                         pow(s.lookfrom.z - s.lookat.z,2) );
                         
    float m_x = s.lookfrom.x 
               + (s.lookat.x - s.lookfrom.x) * td;
               
    float m_y = s.lookfrom.y 
              + (s.lookat.y - s.lookfrom.y) * td;
              
    float m_z = s.lookfrom.z 
            + (s.lookat.z - s.lookfrom.z) * td;
    
    glm::vec3 middle(m_x, m_y, m_z);
    
    // midle of the image plane shifted by cu, cv
    glm::vec3 Pxy = middle + cu*u + cv*v;

    ray.pos = s.lookfrom;
    ray.dir = Pxy - s.lookfrom;
    
    //TODO is this okay for t_min? What is t_max?
    // where it hits image plane to infinity
    ray.t_min = 0.001;
    ray.t_max = 1000000;

}
