
class Scene;
class Ray;
class Sample;

class Camera;

class Camera{
    private:
        Scene s;
        
    public: 
        Camera();
        Camera(Scene &sc);
        ~Camera(){};
        void generateRay(Sample &sample, Ray &ray);
};
