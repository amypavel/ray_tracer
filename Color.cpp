#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"

#include "Color.h"

Color::Color(){
    r = 0; g = 0; b = 0;
}
Color::Color(float red, float green, float blue){
    r = red; g = green; b = blue;
}

// overload + operator for color
Color operator+(const Color &c1, const Color &c2) {
    return Color(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b);
}

// overload - operator for color
Color operator-(const Color &c1, const Color &c2) {
    return Color(c1.r - c2.r, c1.g - c2.g, c1.b - c2.b);
}

// overload * operator for float * color
Color operator*(const float f, const Color &c) {
    return Color(c.r*f, c.g*f, c.b*f);
}
// overload / operator for color / float
Color operator/(const float f, const Color &c) {
    return Color(c.r/f, c.g/f, c.b/f);
}