#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"
#include "FreeImage.h"

#define BPP 24 //Since we're outputting three 8 bit RGB values

using namespace std;

#include "Film.h"
#include "Color.h"
#include "Sample.h"

Film::Film(float w, float h){
    width = w; height = h;
    
    // initializing the buckets
    buckets.resize(width);
    for (int i = 0; i < width; ++i) {
        buckets[i].resize(height);
    }
}

Film::Film(){}

void Film::commit(Sample &sample, Color &color){
    // put a color
    // in the bucket sample.x, sample.y
    buckets[(int)floor(sample.x)][(int)floor(sample.y)] = color;
    
}

void Film::writeImage(string file_name){
    // initialise image
    FreeImage_Initialise();
    FIBITMAP* bitmap = FreeImage_Allocate(width, height, BPP);
    RGBQUAD color;
    if (!bitmap){
        exit(1);
    }
    
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            
            // get color from bucket and color pixel this color
            color.rgbRed = buckets[i][j].r;
            color.rgbGreen = buckets[i][j].g;
            color.rgbBlue = buckets[i][j].b;
            FreeImage_SetPixelColor(bitmap, i, j, &color);
        }
    }
    
    if (FreeImage_Save(FIF_PNG, bitmap, file_name.c_str(), 0 )) {
        cout << "Image successfully saved!" << endl;
        FreeImage_DeInitialise(); // cleanup
    } else {
        cout << "Image did not save." << endl;
    }
}