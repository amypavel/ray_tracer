#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"
using namespace std;

class Sample;
class Color;

// collects colors from the samples and writes them on the image
class Film;
class Film{
private:
    // right now single color per bucket
    // for multiple colors the datastructure should be changed
    float width, height;
    // for now set to max size & r (g, b)
    vector<vector<Color> > buckets;
public: 
    Film(float w, float h);
    Film();
    ~Film(){};
    void commit(Sample &sample, Color &color);
    void writeImage(string file_name);
};