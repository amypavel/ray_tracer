/*
 *  Intersection.cpp
 *  ray_tracer
 */
#include "Shape.h"
#include "LocalGeo.h"
#include "Intersection.h"

Intersection::Intersection(){};

Intersection::Intersection(Shape &s, LocalGeo &l){
    local_geo = l; shape = s;
};