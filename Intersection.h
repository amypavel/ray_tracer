/*
 *  Intersection.h
 */

class Shape;
class LocalGeo;

class Intersection{
    
    public:
    
        Shape shape;
        LocalGeo local_geo;
        
        ~Intersection(){};
        Intersection();
        Intersection(Shape &s, LocalGeo &l);
    
};