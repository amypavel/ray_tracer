/*
 *  LocalGeo.h
 *  ray_tracer
 */

#include "glm/glm.hpp"

class LocalGeo{
    
    public:
    
        glm::vec3 pos;
        glm::vec3 norm;
    
        LocalGeo(glm::vec3 Pos, glm::vec3 Normal);
        ~LocalGeo(){};
        LocalGeo();
};