#Basic Stuff -----------------------------------
CC = g++ -g -Wall -O2 -fmessage-length=0
# Libraries ------------------------------------
CCOPTS = -c
LDOPTS = -L./ -lfreeimage
# final files and intermediate .o files --------
SOURCES = main.cpp Sample.cpp Sampler.cpp Ray.cpp Color.cpp Film.cpp Scene.cpp Camera.cpp RayTracer.cpp Shape.cpp AggregatePrimitive.cpp LocalGeo.cpp Intersection.cpp
OBJECTS = main.o Sample.o Sampler.o Ray.o Color.o Film.o Scene.o Camera.o RayTracer.o Shape.o AggregatePrimitive.o LocalGeo.o Intersection.o
TARGET = main
# -----------------------------------------------
main: $(OBJECTS) 
	$(CC) $(LDOPTS) $(OBJECTS) -o $(TARGET)
main.o: 
	$(CC) $(CCOPTS) $(SOURCES)
default: $(TARGET)

clean:
	/bin/rm -f *.o $(TARGETS)