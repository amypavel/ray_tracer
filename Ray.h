#include <vector>
#include "glm/glm.hpp"

class Ray;

class Ray{
public: 
    glm::vec3 pos, dir;
    float t_min, t_max;
    Ray();
    Ray(glm::vec3 Pos, glm::vec3 Dir);
    ~Ray(){};
};
