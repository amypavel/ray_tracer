/*
 *  RayTracer.cpp
 *  ray_tracer
 *  Copyright 2013 Berkeley. All rights reserved.
 *
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Scene.h"
#include "Color.h"
#include "Ray.h"
#include "Shape.h"

#include "AggregatePrimitive.h"
#include "LocalGeo.h"
#include "Intersection.h"

#include "RayTracer.h"

using namespace std;

RayTracer::RayTracer(){}

RayTracer::RayTracer(Scene &scene_init){
    scene = scene_init;
}

float pdist(glm::vec3 a, glm::vec3 b) {
    return sqrt(pow(a.x-b.x, 2)+
                pow(a.y-b.y, 2)+
                pow(a.z-b.z, 2));
}

void RayTracer::trace(Ray &ray, int depth, Color &color){
    
    if (depth > scene.maxdepth) {
        color.r = 0;
        color.g = 0;
        color.b = 0;
        return;
    }
    
    AggregatePrimitive primitive = AggregatePrimitive(scene);
    
    float thit; Intersection intsec = Intersection();
    
    ray.t_min = 0.001;
    ray.t_max = 10000000;
    
    if (!primitive.intersect(ray, &thit, intsec)) {
        color.r = 0;
        color.g = 0;
        color.b = 0;
        return;
    }
    
    color.r = intsec.shape.emission.x + intsec.shape.ambient.x;
	color.g = intsec.shape.emission.y + intsec.shape.ambient.y;
	color.b = intsec.shape.emission.z + intsec.shape.ambient.z;
    
    for (int m = 0; m < scene.dLightPos.size(); m++) {
        
        Intersection intsec_dl = Intersection();
        float thit_dl;
        Ray dl_ray = Ray(intsec.local_geo.pos,
                         glm::normalize(scene.dLightPos[m]));
        
        dl_ray.t_min = 0.001;
        dl_ray.t_max = 10000000;
        
        if (!primitive.intersect(dl_ray, 
                                 &thit_dl, 
                                 intsec_dl)) {
            
                // normalize L
                glm::vec3 l = dl_ray.dir;
                glm::vec3 n = intsec.local_geo.norm;
                //glm::vec3 viewer = glm::normalize(scene.lookfrom - intsec.local_geo.pos);
                glm::vec3 viewer = glm::normalize(-ray.dir);
                
                // n dot l
                float ndotl = (float)glm::dot(n,l);
                float dterm = max(ndotl,0.0f);
                
                // diffuse term = kd * I * n dot l
                color.r += dterm * intsec.shape.diffuse.x * scene.dLightCol[m].x;
                color.g += dterm * intsec.shape.diffuse.y * scene.dLightCol[m].y;
                color.b += dterm * intsec.shape.diffuse.z * scene.dLightCol[m].z;
                
                // specular term = ks * I * max(r dot v, 0) ^ p
                
                // -l = negative of l
                glm::vec3 negl = glm::vec3(0.0f,0.0f,0.0f) - l;
                
                // find 2*(l dot n)
                float nmult = 2.0f * glm::dot(n,l);
                
                // 2 * (l dot n) * n
                glm::vec3 nprod = nmult * n;
                
                // r = -l + 2 * (l dot n) * n
                glm::vec3 r = nprod + negl;
                
                // max(r dot v, 0) ^ p
                float sterm = pow(max(glm::dot(r,viewer), 0.0f), float(scene.sp));
                sterm = min(sterm, 255.0f);
                
                // ks * I * max(r dot v, 0) ^ p
                color.r += sterm * intsec.shape.specular.x * scene.dLightCol[m].x;
                color.g += sterm * intsec.shape.specular.y * scene.dLightCol[m].y;
                color.b += sterm * intsec.shape.specular.z * scene.dLightCol[m].z;
                
                color.r = min(color.r, 255.0f); 
                color.b = min(color.b, 255.0f);
                color.g = min(color.g, 255.0f); 

            
        }
        
    }
    
    for (int m = 0; m < scene.pLightPos.size(); m++) {
        
        Intersection intsec_pl = Intersection();
        float thit_pl;
        Ray pl_ray = Ray(intsec.local_geo.pos,
                         glm::normalize(scene.pLightPos[m] - intsec.local_geo.pos));
        
        pl_ray.t_min = 0.001;
        pl_ray.t_max = 10000000;
        
        if (!primitive.intersect(pl_ray, 
                                 &thit_pl, 
                                 intsec_pl)
            || pdist(scene.pLightPos[m], 
                     intsec.local_geo.pos) 
            <  pdist(intsec_pl.local_geo.pos, 
                    intsec.local_geo.pos)) {
            
                // normalize L
                //glm::vec3 viewer = glm::normalize(scene.lookfrom - intsec.local_geo.pos); // - intsec.local_geo.pos;
                glm::vec3 viewer = glm::normalize(-ray.dir);
                glm::vec3 l = pl_ray.dir;
                glm::vec3 n = intsec.local_geo.norm;
                //TODO: change hard coded value
                
                // n dot l
                float ndotl = (float)glm::dot(n,l);
                float dterm = max(ndotl,0.0f);
                
                // diffuse term = kd * I * n dot l
                color.r += dterm * intsec.shape.diffuse.x * scene.pLightCol[m].x;
                color.g += dterm * intsec.shape.diffuse.y * scene.pLightCol[m].y;
                color.b += dterm * intsec.shape.diffuse.z * scene.pLightCol[m].z;
                
                
                // specular term = ks * I * max(r dot v, 0) ^ p
                
                // -l = negative of l
                glm::vec3 negl = glm::vec3(0.0f,0.0f,0.0f) - l;
                
                // find 2*(l dot n)
                float nmult = 2.0f * glm::dot(n,l);
                
                // 2 * (l dot n) * n
                glm::vec3 nprod = nmult * n;
                
                // r = -l + 2 * (l dot n) * n
                glm::vec3 r = nprod + negl;
                
                // max(r dot v, 0) ^ p
                float sterm = pow(max(glm::dot(r,viewer), 0.0f), float(scene.sp));
                sterm = min(sterm, 255.0f);
                
                // ks * I * max(r dot v, 0) ^ p
                color.r += sterm * intsec.shape.specular.x * scene.pLightCol[m].x;
                color.g += sterm * intsec.shape.specular.y * scene.pLightCol[m].y;
                color.b += sterm * intsec.shape.specular.z * scene.pLightCol[m].z;
                
                color.r = min(color.r, 255.0f); 
                color.b = min(color.b, 255.0f);
                color.g = min(color.g, 255.0f); 
            
        }
    }
    
     //ref_dir = -ray.dir + 2 * (ray.dir dot n) * n
    glm::vec3 ref_dir = ray.dir - 2.0f 
            * glm::dot(ray.dir, intsec.local_geo.norm)
            * intsec.local_geo.norm;

    glm::vec3 ref_pos = intsec.local_geo.pos;

    Ray reflectRay = Ray(ref_pos, glm::normalize(ref_dir));
    
    reflectRay.t_min = 0.001;
    reflectRay.t_max = 10000000;
    
    Color tempColor = Color(0, 0, 0);
     //Make a recursive call to trace the reflected ray
    trace(reflectRay, depth+1, tempColor);

    color.r += intsec.shape.specular.x * max(tempColor.r, 0.0f);
    color.g += intsec.shape.specular.y * max(tempColor.g, 0.0f);
    color.b += intsec.shape.specular.z * max(tempColor.b, 0.0f);

    color.r = min(color.r, 255.0f);
    color.g = min(color.g, 255.0f);
    color.b = min(color.b, 255.0f);
    
}