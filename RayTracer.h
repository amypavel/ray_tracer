/*
 *  RayTracer.h
 *  ray_tracer
 *
 *  Created by Amy Pavel on 2/25/13.
 *  Copyright 2013 Berkeley. All rights reserved.
 *
 */
class Scene;
class Color;
class Ray;

class RayTracer;

class RayTracer{

    private:
        Scene scene;
    public:
        RayTracer(Scene &scene_init);
        RayTracer();
        ~RayTracer(){};
        void trace(Ray &ray, int depth, Color &color);
};