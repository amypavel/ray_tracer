#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"

#include "Sample.h"

Sample::Sample() {
    x = 0.0; y = 0.0;
}

Sample::Sample(float x_init, float y_init) {
    x = x_init; y = y_init;
}