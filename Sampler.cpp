#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"

#include "Sampler.h"
#include "Sample.h"

Sampler::Sampler() {}
Sampler::Sampler(float w, float h) {
    max_width = w - 1; // max index would be a better name
    max_height = h - 1;
}

bool Sampler::generateSample(Sample &sample) {
    float current_x = floor(sample.x); 
    float current_y = floor(sample.y);
    if (current_x < max_width && current_y <= max_height) {
        sample.x += 1.0;
        return true;
    } else if (current_x == max_width && current_y < max_height) {
        sample.y += 1.0;
        sample.x = 0.0;
        return true;
    } else {
        return false;
    }
}