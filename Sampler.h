// needs the sample class
class Sample;

class Sampler;

class Sampler{
private:
    float max_width, max_height;
public: 
    Sampler(float w, float h);
    Sampler();
    ~Sampler(){};
    bool generateSample(Sample &sample);
};