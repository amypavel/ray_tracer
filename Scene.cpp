#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include <stack>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Scene.h"
#include "Shape.h"

using namespace std;

Scene::Scene() {}
Scene::Scene(string input_file) {
    // Parsing code off of piazza
    // https://piazza.com/class#spring2013/cs184/117
    
    // flag for fov vs corner formatted scene spec
    fov_spec = false; 
    // initialize current transform to identity matrix
    current_trans = glm::mat4(1.0f);
    
    outputfile = "output.bmp";
    ifstream inpfile(input_file.c_str());
    if(!inpfile.is_open()) {
        cout << "Unable to open file: "<< input_file << endl;
    } else {
        string line;
        //MatrixStack mst;
        while(inpfile.good()) {
            vector<string> splitline;
            string buf;
            getline(inpfile,line);
            stringstream ss(line);
            while (ss >> buf) {
                splitline.push_back(buf);
            }
            //Ignore blank lines
            if(splitline.size() == 0) {
                continue;
            }
            //Ignore comments
            if(splitline[0][0] == '#') {
                continue;
            }
            //Valid commands:
            //size width height
            //  must be first command of file, controls image size
            else if(!splitline[0].compare("size")) {
                width = atoi(splitline[1].c_str());
                height = atoi(splitline[2].c_str());
            }
            //maxdepth depth
            //  max # of bounces for ray (default 5)
            else if(!splitline[0].compare("maxdepth")) {
                maxdepth = atoi(splitline[1].c_str());
            }
            //output filename
            //  output file to write image to 
            else if(!splitline[0].compare("output")) {
                outputfile = splitline[1];
            }
            //camera lookfromx lookfromy lookfromz lookatx lookaty lookatz upx upy upz fov
            //  speciﬁes the camera in the standard way, as in homework 2.
            else if(!splitline[0].compare("camera")) {
                // lookfrom:
                lfromx = atof(splitline[1].c_str());
                lfromy = atof(splitline[2].c_str());
                lfromz = atof(splitline[3].c_str());
                // lookat:
                latx = atof(splitline[4].c_str());
                laty = atof(splitline[5].c_str());
                latz = atof(splitline[6].c_str());
                // up:
                upx = atof(splitline[7].c_str());
                upy = atof(splitline[8].c_str());
                upz = atof(splitline[9].c_str());
                // field of view vertical: 
                fovv = atof(splitline[10].c_str());
                fov_spec = true;
            } 
            
            else if(!splitline[0].compare("vertex")){
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                glm::vec3 vertex(v1, v2, v3);
                verticies.push_back(vertex);
            } 
            
            else if(!splitline[0].compare("tri")){
                // what does atof mean
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                // make a triangle out of three verticies
                tri_v1.push_back(verticies[(int)v1]);
                tri_v2.push_back(verticies[(int)v2]);
                tri_v3.push_back(verticies[(int)v3]);
                
                // TODO: add ability to push and pop transforms
                tri_inv.push_back(current_inv);
                tri_emission.push_back(emission);
                tri_ambient.push_back(ambient);
                tri_diffuse.push_back(diffuse);
                tri_specular.push_back(specular);
                tri_reflect.push_back(reflect);
                
            } 
            
            else if(!splitline[0].compare("sphere")){
                s_x = atof(splitline[1].c_str());
                s_y= atof(splitline[2].c_str());
                s_z= atof(splitline[3].c_str());
                s_rad = atof(splitline[4].c_str());
                
                spheres.push_back(glm::vec4(s_x, s_y, s_z, s_rad));
                
                // TODO: add ability to push and pop transforms
                sphere_inv.push_back(current_inv);
                sphere_emission.push_back(emission);
                sphere_ambient.push_back(ambient);
                sphere_diffuse.push_back(diffuse);
                sphere_specular.push_back(specular);
                sphere_reflect.push_back(reflect);
                
            } 
            
            else if(!splitline[0].compare("translate")){
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                
                glm::mat4 trans = 
                	glm::translate(glm::mat4(1.0f), 
                	         glm::vec3(v1, v2, v3));
                
                glm::mat4 inv_trans = 
                    glm::translate(glm::mat4(1.0f), 
                                   glm::vec3(-v1, -v2, -v3));
                
                current_trans = trans * current_trans;
                current_inv = inv_trans *current_inv ;
            }
            
            else if(!splitline[0].compare("scale")){
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                
                glm::mat4 scl = 
                	glm::scale(glm::mat4(1.0f), glm::vec3(v1,v2,v3));
                
                float inv_v1, inv_v2, inv_v3; 
                
                if (v1 == 0.0f) {inv_v1 = 0.0f;} else {inv_v1 = 1.0/v1;}
                if (v2 == 0.0f) {inv_v2 = 0.0f;} else {inv_v2 = 1.0/v2;}
                if (v3 == 0.0f) {inv_v3 = 0.0f;} else {inv_v3 = 1.0/v3;}
                
                glm::mat4 inv_scl = 
                    glm::scale(glm::mat4(1.0f), glm::vec3(inv_v1,inv_v2,inv_v3));
                
                current_trans = scl * current_trans;
                current_inv = inv_scl * current_inv;
            }
            else if(!splitline[0].compare("reflect")){
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                
                reflect = glm::vec3(v1, v2, v3);
            }
            
            else if(!splitline[0].compare("rotate")){
                float v1 = atof(splitline[1].c_str());
                float v2 = atof(splitline[2].c_str());
                float v3 = atof(splitline[3].c_str());
                float angle = atof(splitline[4].c_str());
                glm::mat4 identity = glm::mat4(1.0f);
                
                // TODO: figure out why this doesn't work when v1,v2,v3 all = 0
                // TODO: figure out if this works at all
                glm::mat4 rot = glm::rotate(identity, 
                                        angle, 
                            	        glm::vec3(v1, v2, v3));
                
                glm::mat4 inv_rot = glm::rotate(identity, 
                                            -angle, 
                                            glm::vec3(v1, v2, v3));
       	        
                current_trans = rot * current_trans;
                current_inv = inv_rot * current_inv;
            }
            
            else if(!splitline[0].compare("pushTransform")){
                current_inv = glm::mat4(1.0f);
            }
            
            else if(!splitline[0].compare("popTransform")){
                // TODO: add ability to pop transforms
                current_inv = glm::mat4(1.0f);
            }
            
            else if (!(splitline[0].compare("directional"))){
    	    	float x = atof(splitline[1].c_str());
    	    	float y = atof(splitline[2].c_str());
    	    	float z = atof(splitline[3].c_str());
    	    	float r = atof(splitline[4].c_str());
    	    	float g = atof(splitline[5].c_str());
    	    	float b = atof(splitline[6].c_str());
        		//DirectionalLight d(x, y, z, r, g, b);

        		glm::vec3 pos(x, y, z);
        		glm::vec3 col(r, g, b);
    
                col = col * 255.0f;
        		dLightPos.push_back(pos);
       
        		dLightCol.push_back(col);
              
    	    }

    	    else if (!splitline[0].compare("ambient")) {

    	    	float r = atof(splitline[1].c_str());
    	    	float g = atof(splitline[2].c_str());
    	    	float b = atof(splitline[3].c_str());
    	  	    ambient = glm::vec3(r, g, b);
                ambient = ambient * 255.0f;
    	    } 
            
            else if (!splitline[0].compare("emission")) {
    	    	float r = atof(splitline[1].c_str());
    	    	float g = atof(splitline[2].c_str());
    	    	float b = atof(splitline[3].c_str());
    	  	    emission = glm::vec3(r, g, b);
                emission = emission * 255.0f;

    	    } 
    	    
    	    else if (!splitline[0].compare("diffuse")) {

    	    	float r = atof(splitline[1].c_str());
    	    	float g = atof(splitline[2].c_str());
    	    	float b = atof(splitline[3].c_str());

    	  	    diffuse = glm::vec3(r, g, b);
    	    } 
            
            else if (!splitline[0].compare("shininess")) {
    	    	sp = atof(splitline[1].c_str());
    	    } 
    	    
    	    else if (!splitline[0].compare("specular")) {
           
    	    	float r = atof(splitline[1].c_str());
    	    	float g = atof(splitline[2].c_str());
    	    	float b = atof(splitline[3].c_str());
    	  	    specular = glm::vec3(r, g, b);
    	    } 
            
            else if (!(splitline[0].compare("point"))){
            
                float x = atof(splitline[1].c_str());
                float y = atof(splitline[2].c_str());
                float z = atof(splitline[3].c_str());
                float r = atof(splitline[4].c_str());
                float g = atof(splitline[5].c_str());
                float b = atof(splitline[6].c_str());
                //DirectionalLight d(x, y, z, r, g, b);
                glm::vec3 pos(x, y, z);
                glm::vec3 col(r, g, b);

                col = 255.0f * col;
                pLightPos.push_back(pos);
                pLightCol.push_back(col);
         
            }
            
            
            else {
                cerr << "Unknown command: " << splitline[0] << endl;
            }
        }
        inpfile.close();
        
        if (fov_spec) {
            // store lookfrom, lookat and fov vectors
            glm::vec3 lfrom(lfromx, lfromy, lfromz);
            glm::vec3 lat(latx, laty, latz);
            glm::vec3 up_init(upx, upy, upz); 
            
            up = glm::normalize(up_init);
            lookat = lat; lookfrom = lfrom;
        } 
        
        
        
    }
}
