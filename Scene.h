#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include "glm/glm.hpp"
#include <stack>

using namespace std;

class Triangle;
class Sphere;
class Scene;

class Scene{
private:
    float s;
public: 
    Scene(string inputfile);
    Scene();
    ~Scene(){};
    
    float width, height, maxdepth;
    
    string outputfile;
    
    // lookat = lat, lookfrom = lfrom 
    float latx, laty, latz, lfromx, lfromy, lfromz;
    
    // up = up, field of view, vert = fovv
    // field of view, horz = fovh
    float upx, upy, upz, fovv, fovh;

    //sphere center coordinates/radius
    float s_x, s_y, s_z, s_rad;
    
    //store the verticies defined 'vertex 1 -1 1'
    vector<glm::vec3> verticies;
    glm::mat4 current_trans; //current transform
    vector<glm::mat4> transforms;
    
    glm::mat4 current_inv; //current transform
    vector<glm::mat4> inverses;
    
    // TODO: make these SHAPE vector instead
    vector<glm::vec4> spheres;
    vector<glm::mat4> sphere_inv;
    vector<glm::vec3> sphere_emission;
    vector<glm::vec3> sphere_ambient;
    vector<glm::vec3> sphere_diffuse;
    vector<glm::vec3> sphere_specular;
    vector<glm::vec3> sphere_reflect;
    
    // TODO: make these SHAPE vectors instead
    vector<glm::vec3> tri_v1;
    vector<glm::vec3> tri_v2;
    vector<glm::vec3> tri_v3;
    vector<glm::mat4> tri_inv;
    vector<glm::vec3> tri_emission;
    vector<glm::vec3> tri_ambient;
    vector<glm::vec3> tri_diffuse;
    vector<glm::vec3> tri_specular;
    vector<glm::vec3> tri_reflect;

    vector<glm::vec3> dLightCol;
    vector<glm::vec3> dLightPos;
    vector<glm::vec3> pLightCol;
    vector<glm::vec3> pLightPos;
    glm::vec3 emission, ambient, diffuse, specular, reflect;
    float sp;
    
    glm::vec3 lookat, lookfrom, up;
    
    // spec format
    bool fov_spec;
};
