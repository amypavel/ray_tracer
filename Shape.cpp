#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>

#include "Shape.h"
#include "Ray.h"
#include "LocalGeo.h"
#include "Intersection.h"

#include "glm/glm.hpp"
using namespace std;

Shape::Shape(){}

Sphere::Sphere() {
    x = 0; y = 0; z = 0; r = 0; inv = glm::mat4(1.0f);
}

Sphere::Sphere(float X, float Y, float Z, float R) {
    x = X; y = Y; z = Z; r = R; inv = glm::mat4(1.0f);
}

Sphere::Sphere(glm::vec4 spec, glm::mat4 i, 
               glm::vec3 Emission,
               glm::vec3 Ambient,
               glm::vec3 Diffuse,
               glm::vec3 Specular,
               glm::vec3 Reflect) {
    
    x = spec[0]; y = spec[1]; z = spec[2]; r = spec[3]; 
    inv = i;
    emission = Emission;
    ambient = Ambient;
    diffuse = Diffuse;
    specular = Specular;
    reflect = Reflect;

}

bool Sphere::intersect(Ray ray, float *thit, LocalGeo &loc){
    
    // homogenous coordinates 
    glm::vec4 h_dir(ray.dir[0], ray.dir[1], ray.dir[2], 0);
    glm::vec4 h_pos(ray.pos[0], ray.pos[1], ray.pos[2], 1);
    
    // left multiply by inverse to find ray
    glm::vec4 inv_pos = inv * h_pos;
    glm::vec4 inv_dir = inv * h_dir;
    
    // unhomogenous for ray creation
    // TODO: see if this conversion is okay
    glm::vec3 inv_p(inv_pos[0]/(float) inv_pos[3], 
                    inv_pos[1]/(float) inv_pos[3], 
                    inv_pos[2]/(float) inv_pos[3]);
    
    glm::vec3 inv_d(inv_dir[0], 
                    inv_dir[1], 
                    inv_dir[2]);
    
    Ray trans_ray = Ray(inv_p, inv_d);
    
    glm::vec3 center(x, y, z);
    glm::vec3 e_c = trans_ray.pos - center;
    
    float dd = glm::dot(trans_ray.dir,trans_ray.dir);
    float b_squared = pow(glm::dot(trans_ray.dir, (trans_ray.pos - center)), 2);
    
    float four_ac = dd * (glm::dot(e_c, e_c) - pow(r, 2));
    
    float disc = b_squared - four_ac;
    float neg_b = glm::dot(-1.0f * trans_ray.dir, e_c);
    
    if (disc < 0 ) {
        
        return false;
        
    } else {
        float t0 = (neg_b - sqrt(disc))/dd;
        float t1 = (neg_b + sqrt(disc))/dd;
        
        if (t0 < t1 && t0 < ray.t_max && t0 > ray.t_min) {

            float x0 = trans_ray.pos.x + t0 * trans_ray.dir.x;
            float y0 = trans_ray.pos.y + t0 * trans_ray.dir.y;
            float z0 = trans_ray.pos.z + t0 * trans_ray.dir.z;
            thit = &t0;
            
            // inverse matrix transposed to find norm (0 for trasnforming direction)
            glm::vec4 h_norm_os((x0-x)/r, (y0-y)/r, (z0-z)/r, 0);
            glm::mat4 invt = glm::transpose(inv);
            glm::vec4 h_norm_ws = invt * h_norm_os;
            
            // norm in world space
            loc.norm = glm::normalize(glm::vec3(h_norm_ws[0], 
                                                h_norm_ws[1], 
                                                h_norm_ws[2]));
            
            // obj to world * position = position in world space
            glm::vec4 h_pos_os(x0, y0, z0, 1.0f);
            glm::mat4 os_to_ws = glm::inverse(inv);
            glm::vec4 h_pos_ws = os_to_ws * h_pos_os;
            
            // position in world space
            loc.pos = glm::vec3(h_pos_ws[0]/h_pos_os[3], 
                                h_pos_ws[1]/h_pos_os[3], 
                                h_pos_ws[2]/h_pos_os[3]);
            
            // Old commands -- 
            //loc.norm = glm::vec3((x0-x)/r, (y0-y)/r, (z0-z)/r);
            //loc.pos = glm::vec3(x0, y0, z0); 
            
            return true;
        }
        else if (t1 >= t0 && t1 < ray.t_max && t1 > ray.t_min) {
            
            float x0 = trans_ray.pos.x + t1 * trans_ray.dir.x;
            float y0 = trans_ray.pos.y + t1 * trans_ray.dir.y;
            float z0 = trans_ray.pos.z + t1 * trans_ray.dir.z;
            thit = &t1;
            
            // inverse matrix transposed to find norm (0 for trasnforming direction)
            glm::vec4 h_norm_os((x0-x)/r, (y0-y)/r, (z0-z)/r, 0);
            glm::mat4 invt = glm::transpose(inv);
            glm::vec4 h_norm_ws = invt * h_norm_os;
            
            // norm in world space
            loc.norm = glm::normalize(glm::vec3(h_norm_ws[0], 
                                                h_norm_ws[1], 
                                                h_norm_ws[2]));
            
            // obj to world * position = position in world space
            glm::vec4 h_pos_os(x0, y0, z0, 1.0f);
            glm::mat4 os_to_ws = glm::inverse(inv);
            glm::vec4 h_pos_ws = os_to_ws * h_pos_os;
            
            // position in world space
            loc.pos = glm::vec3(h_pos_ws[0]/h_pos_os[3], 
                                h_pos_ws[1]/h_pos_os[3], 
                                h_pos_ws[2]/h_pos_os[3]);
            
            // Old commands -- 
            //loc.norm = glm::vec3((x0-x)/r, (y0-y)/r, (z0-z)/r);
            //loc.pos = glm::vec3(x0, y0, z0); 
            
            return true;
        } else {
            return false;
        }

    }
}

Triangle::Triangle() {
	v1.x = 0;
	v2.x = 0;
	v3.x = 0;
	v1.y = 0;
	v2.y = 0;
	v3.y = 0;
	v1.z = 0;
	v2.z = 0;
	v3.z = 0;
}

Triangle::Triangle(glm::vec3 V1, glm::vec3 V2, glm::vec3 V3) {
	v1 = V1;
	v2 = V2;
	v3 = V3;
}

Triangle::Triangle(glm::vec3 V1, glm::vec3 V2, glm::vec3 V3, 
                   glm::mat4 Inv,
                   glm::vec3 Emission,
                   glm::vec3 Ambient,
                   glm::vec3 Diffuse,
                   glm::vec3 Specular,
                   glm::vec3 Reflect) {
	v1 = V1;
	v2 = V2;
	v3 = V3;
    inv = Inv;
    emission = Emission;
    ambient = Ambient;
    diffuse = Diffuse;
    specular = Specular;
    reflect = Reflect;
    
}

bool Triangle::intersect(Ray ray, float *thit, LocalGeo &loc) {
    
    // homogenous coordinates 
    glm::vec4 h_dir(ray.dir[0], ray.dir[1], ray.dir[2], 0);
    glm::vec4 h_pos(ray.pos[0], ray.pos[1], ray.pos[2], 1);
    
    // left multiply by inverse to find ray
    glm::vec4 inv_pos = inv * h_pos;
    glm::vec4 inv_dir = inv * h_dir;
    
    // unhomogenous for ray creation
    // TODO: see if this conversion is okay
    glm::vec3 inv_p(inv_pos[0]/(float) inv_pos[3], 
                    inv_pos[1]/(float) inv_pos[3], 
                    inv_pos[2]/(float) inv_pos[3]);
    
    glm::vec3 inv_d(inv_dir[0], 
                    inv_dir[1], 
                    inv_dir[2]);
    
    Ray trans_ray = Ray(inv_p, inv_d);
    
    glm::vec3 dir = trans_ray.dir;
    float t_min = ray.t_min;
    float t_max = ray.t_max;  
    
    float a, b, c, d, e, f, g, h, i, j, k, l, M, beta, gamma, t;
    a = v1.x - v2.x;
    b = v1.y - v2.y;
    c = v1.z - v2.z;
    
    d = v1.x - v3.x;
    e = v1.y - v3.y;
    f = v1.z - v3.z;
    
    g = dir.x;
    h = dir.y;
    i = dir.z;
    
    j = v1.x - trans_ray.pos.x;
    k = v1.y - trans_ray.pos.y;
    l = v1.z - trans_ray.pos.z;
    
    M = a * (e*i - h*f) + b*(g*f-d*i) + c*(d*h - e*g);
    
    gamma = (i*(a*k-j*b) + h*(j*c - a*l) + g*(b*l - k*c))/M;
    if (gamma < 0 || gamma > 1) {
        return false;
    }
    
    beta = (j*(e*i-h*f) + k*(g*f-d*i)+ l*(d*h-e*g))/M;
    if (beta < 0 || (beta > 1 - gamma)) {
        return false;
    }
    
    t = -(f*(a*k-j*b) + e*(j*c - a*l) + d*(b*l - k*c))/M;
    if (t < t_min || t > t_max) {
        return false;
    }
    
	thit = &t;
	
	//Note: alpha = 1 - beta - gamma
	//float alpha = 1 - beta - gamma;
    
	float x0 = v1.x + beta* (v2.x - v1.x) + gamma * (v3.x - v1.x);
	float y0 = v1.y + beta* (v2.y - v1.y) + gamma * (v3.y - v1.y);
	float z0 = v1.z + beta* (v2.z - v1.z) + gamma * (v3.z - v1.z);
    
	glm::vec3 pos(x0, y0, z0);
    
	glm::vec3 normal = glm::cross((v2 - v1), (v3 - v1)); 
    
    // ---
    // inverse matrix transposed to find norm (0 for trasnforming direction)
    glm::vec4 h_norm_os(normal[0], normal[1], normal[2], 0);
    glm::mat4 invt = glm::transpose(inv);
    glm::vec4 h_norm_ws = invt * h_norm_os;
    
    // norm in world space
    loc.norm = glm::normalize(glm::vec3(h_norm_ws[0], 
                                        h_norm_ws[1], 
                                        h_norm_ws[2]));
    
    // obj to world * position = position in world space
    glm::vec4 h_pos_os(x0, y0, z0, 1.0f);
    glm::mat4 os_to_ws = glm::inverse(inv);
    glm::vec4 h_pos_ws = os_to_ws * h_pos_os;
    
    // position in world space
    loc.pos = glm::vec3(h_pos_ws[0]/h_pos_os[3], 
                        h_pos_ws[1]/h_pos_os[3], 
                        h_pos_ws[2]/h_pos_os[3]);
    
    // Old commands -- 
    //loc.norm = glm::vec3((x0-x)/r, (y0-y)/r, (z0-z)/r);
    //loc.pos = glm::vec3(x0, y0, z0); 
    // ---
	
    return true;
}
