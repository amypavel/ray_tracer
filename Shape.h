#include "glm/glm.hpp"

class Ray;
class LocalGeo;

class Shape{
public:
    glm::mat4 inv; 
    glm::vec3 emission, ambient, diffuse, specular, reflect;
    Shape();
    ~Shape(){};
};

class Sphere: public Shape{
public: 
    float x, y, z, r;
    glm::mat4 inv;
    Sphere();
    ~Sphere(){};
    Sphere(float X, float Y, float Z, float R);
    Sphere(glm::vec4 spec, glm::mat4 m, 
           glm::vec3 Emission,
           glm::vec3 Ambient,
           glm::vec3 Diffuse,
           glm::vec3 Specular,
           glm::vec3 Reflect);
    bool intersect(Ray ray, float *thit, LocalGeo &loc);
};

class Triangle: public Shape{
public:
	glm::vec3 v1, v2, v3;
    glm::mat4 inv;
	Triangle();
	Triangle(glm::vec3 V1, glm::vec3 V2, glm::vec3 V3);
    
    Triangle(glm::vec3 V1, glm::vec3 V2, 
                glm::vec3 V3, glm::mat4 Inv, 
                glm::vec3 Emission,
                glm::vec3 Ambient,
                glm::vec3 Diffuse,
                glm::vec3 Specular,
                glm::vec3 Reflect);
    
	~Triangle(){};
	bool intersect(Ray ray, float *thit, LocalGeo &loc);
};