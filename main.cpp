#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>
#include "glm/glm.hpp"
#include "FreeImage.h"
using namespace std;

// Classes for raytracer
#include "Scene.h"
#include "Sample.h"
#include "Sampler.h"
#include "Ray.h"
#include "Color.h"
#include "Film.h"
#include "Camera.h"
#include "RayTracer.h"
#include "Shape.h"
#include "LocalGeo.h"
#include "Intersection.h"
#include "AggregatePrimitive.h"

//***********************
// Global variables
//***********************

Scene scene;


//***********************
// Handy functions
//***********************

// this handy function converts string to float
float to_float ( const char *p )
{
    stringstream ss ( p );
    float result = 0;
    ss>> result;
    return result;
}

//********************
// Classes
//*******************

// now in separate files

// implementation strategy: http://inst.eecs.berkeley.edu/~cs184/fa09/raytrace_journal.php
// partial spec: http://inst.eecs.berkeley.edu/~cs184/fa09/resources/raytracing.htm

//***************
// main etc
//**************
int main(int argc, char *argv[]) {
    // TODO -- fail gracefully if input file is not given
    string input_file = argv[1];
    
    scene = Scene(input_file);
    // 1 sample in the middle of each pixel
    // x val will increase to 0.5 on first generateSample
    Sample sample = Sample(-0.5,0.5); 
    Sampler sampler = Sampler(scene.width, scene.height);
    Color clr = Color(255,255,255);
    Film film = Film(scene.width, scene.height);
    Camera camera = Camera(scene);
    Ray ray = Ray();
    RayTracer raytracer = RayTracer(scene);
    
    while (sampler.generateSample(sample)) {
        //cout << sample.x << "  " << sample.y << endl;
        camera.generateRay(sample, ray);
        raytracer.trace(ray, 0, clr);
        film.commit(sample, clr);
    }
    
    film.writeImage(scene.outputfile);

}
